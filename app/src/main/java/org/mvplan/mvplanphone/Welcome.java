/*
 * Welcome.java
 *
 *   This program is part of MV-Plan
 *   (c) 2006 Guy Wittig <>
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */

package org.mvplan.mvplanphone;
import org.mvplan.mvplanphone.preferences.SharedPreferencesDAO;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;

public class Welcome extends mvPlanPhoneActivity {
	Button b;
	Context c ;
	CheckBox  showLater;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		c = this;
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		final SharedPreferencesDAO sh = new SharedPreferencesDAO(prefs);
		boolean sl = sh.getShowLater();
		if (!sl){
			open();
			finish();
		}
		setContentView(R.layout.welcome);
		
		showLater = (CheckBox)findViewById(R.id.welcome_show_later);
		showLater.setChecked(sl);

		b =  (Button) findViewById(R.id.ButtonOpenPlanner);
		
		b.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				showLater = (CheckBox)findViewById(R.id.welcome_show_later);
				sh.setShowLater(showLater.isChecked());
				open();
				finish();
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		final SharedPreferencesDAO sh = new SharedPreferencesDAO(prefs);
		boolean sl = sh.getShowLater();
		if (!sl){
			open();
			finish();
		}
	}
}