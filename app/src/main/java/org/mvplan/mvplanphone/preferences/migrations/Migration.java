package org.mvplan.mvplanphone.preferences.migrations;

import android.content.SharedPreferences;

public interface Migration {
	void migrate(SharedPreferences prefs);
}
