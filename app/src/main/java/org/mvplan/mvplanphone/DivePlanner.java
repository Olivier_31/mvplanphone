/*
 * DivePlanner.java
 *
 *   This program is part of MV-Plan
 *   (c) 2006 Guy Wittig <>
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */

package org.mvplan.mvplanphone;

import mvplan.main.MvplanInstance;
import mvplan.prefs.Prefs;
import mvplan.prefs.PrefsException;

import org.mvplan.mvplanphone.gui.MVPlanPreferences;
import org.mvplan.mvplanphone.preferences.SharedPreferencesDAO;

import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class DivePlanner extends ActivityGroup {
	LinearLayout view;
	Window gasListAction;
	Window segmentAction;
	Window diveAction;
	Window calculateAction;
	Window prefsAction;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getPrefs();
		setContentView(R.layout.dive_planner_view);
		view = (LinearLayout) findViewById(R.id.testme);
		LocalActivityManager localActivityManager = getLocalActivityManager();
		gasListAction = localActivityManager.startActivity("gasListAction",
				new Intent(this, GasList.class));
		segmentAction = localActivityManager.startActivity("segmentAction",
				new Intent(this, SegmentList.class));
		diveAction = localActivityManager.startActivity("diveAction",
				new Intent(this, DiveList.class));
		calculateAction = localActivityManager.startActivity("calculateAction",
				new Intent(this, ProfileView.class));
		prefsAction = localActivityManager.startActivity("prefsAction",
				new Intent(this, MVPlanPreferences.class));

		getPrefs();
/*		view.addView(gasListAction.getDecorView(), LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		GasList g = (GasList) (DivePlanner.this.getLocalActivityManager().getActivity("gasListAction"));
		view.setContentDescription("GasList");
		g.reloadAdaptor();
*/		view.addView(calculateAction.getDecorView(), LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		ProfileView p = (ProfileView) (DivePlanner.this.getLocalActivityManager().getActivity("calculateAction"));
		view.setContentDescription("DiveProfile");
		p.calculate();

		ImageButton ib = (ImageButton) findViewById(R.id.dive_planner_gasList);
		ib.setOnClickListener(gasListButtonListener);

		ib = (ImageButton) findViewById(R.id.dive_planner_segment);
		ib.setOnClickListener(segmentButtonListener);

		ib = (ImageButton) findViewById(R.id.dive_planner_dive);
		ib.setOnClickListener(diveButtonListener);

		ib = (ImageButton) findViewById(R.id.dive_planner_calculate);
		ib.setOnClickListener(calculateButtonListener);

		ib = (ImageButton) findViewById(R.id.dive_planner_prefs);
		ib.setOnClickListener(prefsButtonListener);
	}

	android.view.View.OnClickListener gasListButtonListener = new android.view.View.OnClickListener() {
		public void onClick(View v) {
			savePrefs();
			view.removeAllViews();
			view.addView(gasListAction.getDecorView(), LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			GasList g = (GasList) (DivePlanner.this.getLocalActivityManager().getActivity("gasListAction"));
			view.setContentDescription("GasList");
			g.reloadAdaptor();
		}
	};
	android.view.View.OnClickListener segmentButtonListener = new android.view.View.OnClickListener() {
		public void onClick(View v) {
			savePrefs();
			view.removeAllViews();
			view.addView(segmentAction.getDecorView(), LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			SegmentList s = (SegmentList) (DivePlanner.this.getLocalActivityManager().getActivity("segmentAction"));
			view.setContentDescription("SegmentList");
			s.reloadAdaptor();
		}
	};
	android.view.View.OnClickListener diveButtonListener = new android.view.View.OnClickListener() {
		public void onClick(View v) {
			String S = view.getContentDescription().toString();
			if (S == "Settings") getPrefs();
			else savePrefs();
			view.removeAllViews();
			view.addView(diveAction.getDecorView(), LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			DiveList s = (DiveList) (DivePlanner.this.getLocalActivityManager().getActivity("diveAction"));
			view.setContentDescription("DiveList");
			s.reloadAdaptor();
		}
	};
	android.view.View.OnClickListener calculateButtonListener = new android.view.View.OnClickListener() {
		public void onClick(View v) {
			String S = view.getContentDescription().toString();
			if (S == "Settings") getPrefs();
			else savePrefs();
			view.removeAllViews();
			view.addView(calculateAction.getDecorView(), LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			ProfileView p = (ProfileView) (DivePlanner.this.getLocalActivityManager().getActivity("calculateAction"));
			view.setContentDescription("Calculations");
			p.calculate();
		}
	};
	android.view.View.OnClickListener prefsButtonListener = new android.view.View.OnClickListener() {
		public void onClick(View v) {
			savePrefs();
			view.removeAllViews();
			view.addView(prefsAction.getDecorView(), LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			MVPlanPreferences p = (MVPlanPreferences) (DivePlanner.this.getLocalActivityManager().getActivity("gui.MVPlanPreferences"));
			view.setContentDescription("Settings");
		}
	};

	@Override
	protected void onPause() {
		super.onPause();
		savePrefs();
	};
	
	@Override
	protected void onResume() {
		super.onResume();
		getPrefs();
	}

	private void getPrefs() {
		// Get the xml/preferences.xml preferences
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
		SharedPreferencesDAO dao = new SharedPreferencesDAO (prefs);
		try {
			Prefs loadPrefs = dao.loadPrefs();
		} catch (PrefsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void savePrefs() {
		// Get the xml/preferences.xml preferences
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
		SharedPreferencesDAO dao = new SharedPreferencesDAO (prefs);
		try {
			Prefs mvPrefs = MvplanInstance.getPrefs();
			dao.savePrefs(mvPrefs);
		} catch (PrefsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
