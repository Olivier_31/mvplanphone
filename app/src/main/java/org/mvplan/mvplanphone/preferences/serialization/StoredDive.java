/*
 * StoredDive.java
 *
 *   This program is part of MV-Plan
 *   (c) 2006 Guy Wittig <>
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */

package org.mvplan.mvplanphone.preferences.serialization;

import java.util.List;

import mvplan.gas.Gas;
import mvplan.dives.DiveAbstract;
import mvplan.dives.DiveDive;

public class StoredDive {

	double depth = 0.0;
	Boolean enable = true;
	int gas = -1;
	double setpoint = 0.0;
	double time = 0.0;

	public double getDepth() {
		return depth;
	}

	public Boolean getEnable() {
		return enable;
	}

	public int getGas() {
		return gas;
	}

	public double getSetpoint() {
		return setpoint;
	}

	public double getTime() {
		return time;
	}

	public void setDepth(double depth) {
		this.depth = depth;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	public void setGas(int gas) {
		this.gas = gas;
	}

	public void setSetpoint(double setpoint) {
		this.setpoint = setpoint;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public StoredDive(DiveAbstract s , List<Gas> knownGases) {
		depth = s.getDepth();
		enable = s.getEnable();
		setpoint = s.getSetpoint();
		time = s.getTime();
		Gas g = s.getGas();
		for(int i = 0 ; i < knownGases.size(); i++){
			if (0 == knownGases.get(i).compareTo(g)) {
				gas = i;
				break;
			}
		}
	}
	public DiveAbstract toDive(List<Gas> knownGases){
		DiveDive ret = new DiveDive(depth, time,knownGases.get(gas), setpoint);
		ret.setEnable(enable);
		return ret;
		
	}
}
