/*
 * DiveDialog.java
 *
 *   This program is part of MV-Plan
 *   (c) 2006 Guy Wittig <>
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */


package org.mvplan.mvplanphone.preferences.serialization;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import mvplan.gas.Gas;
import mvplan.prefs.Prefs;
import mvplan.segments.SegmentAbstract;
import mvplan.dives.DiveAbstract;

public class PrefsSerialization {
	Gson gson;
	private Prefs prefs;
	
	public PrefsSerialization(Prefs prefs) {
		this.prefs = prefs;
		gson = new Gson();
	}
	
	
	public String serializeGases(){
		List<Gas> gases = prefs.getPrefGases();
		if (gases == null  || gases.size()==0) return null;
		
		List<StoredGas> sgases = new ArrayList<StoredGas>() ;
		for (Gas gas : gases) {
			sgases.add(new StoredGas(gas));
		}
		return gson.toJson(sgases);
	}
	public void deserializeGases(String gases) {
		List<StoredGas> sgases = gson.fromJson(gases, new TypeToken<List<StoredGas>>(){}.getType());
		ArrayList<Gas> g = new ArrayList<Gas>();
		for (StoredGas storedGas : sgases) {
			g.add(storedGas.toGas());
		}
		prefs.setPrefGases(g);
	}

	/**
	 * Deserialize segments, remember to first deserialize gases.
	 *
	 * @param segments the gases
	 */
	public void deserializeSegments(String segments) {
		List<StoredSegment> sseg = gson.fromJson(segments, new TypeToken<List<StoredSegment>>(){}.getType());
		ArrayList<SegmentAbstract> g = new ArrayList<SegmentAbstract>();
		for (StoredSegment storedGas : sseg) {
			g.add(storedGas.toSegment(prefs.getPrefGases()));
		}
		prefs.setPrefSegments(g);
	}


	public String serializeSegments(){
		List<SegmentAbstract> segments = prefs.getPrefSegments();
		if (segments == null  || segments.size()==0) return null;

		List<StoredSegment> ss = new ArrayList<StoredSegment>() ;
		for (SegmentAbstract s : segments) {
			ss.add(new StoredSegment(s,prefs.getPrefGases()));
		}
		return gson.toJson(ss);
	}

	/**
	 * Deserialize dives, remember to first deserialize gases.
	 *
	 * @param dives the gases
	 */
	public void deserializeDives(String dives) {
		List<StoredDive> sseg = gson.fromJson(dives, new TypeToken<List<StoredDive>>(){}.getType());
		ArrayList<DiveAbstract> g = new ArrayList<DiveAbstract>();
		for (StoredDive storedGas : sseg) {
			g.add(storedGas.toDive(prefs.getPrefGases()));
		}
		prefs.setPrefDives(g);
	}


	public String serializeDives(){
		List<DiveAbstract> dives = prefs.getPrefDives();
		if (dives == null  || dives.size()==0) return null;

		List<StoredDive> ss = new ArrayList<StoredDive>() ;
		for (DiveAbstract s : dives) {
			ss.add(new StoredDive(s,prefs.getPrefGases()));
		}
		return gson.toJson(ss);
	}



}
