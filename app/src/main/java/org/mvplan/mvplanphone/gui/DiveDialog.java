/*
 * DiveDialog.java
 *
 *   This program is part of MV-Plan
 *   (c) 2006 Guy Wittig <>
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */

package org.mvplan.mvplanphone.gui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.mvplan.mvplanphone.R;

import java.util.ArrayList;
import java.util.List;

import mvplan.gas.Gas;
import mvplan.main.MvplanInstance;
import mvplan.dives.DiveAbstract;
import mvplan.dives.DiveDive;

public class DiveDialog extends Dialog {
	DiveDialogCallback callback;
	DiveAbstract dive;

	EditText depth;
	EditText time;
	EditText setPoint;
	Spinner gases;

	Button ok;
	Button cancel;

	public DiveDialog(Context context, DiveDialogCallback callback) {
		super(context);
		dive = new DiveDive(0, 0, null, 0);
		this.callback = callback;

	}

	public DiveDialog(Context context, DiveAbstract dive,
                      DiveDialogCallback callback) {
		super(context);
		this.callback = callback;
		this.dive = (DiveAbstract) dive.clone();
	}

	public DiveDialog(Context context, DiveAbstract dive) {
		super(context);
		this.dive = dive;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.dive_dialog);
		setTitle(R.string.dive_dialog_title);
		setCancelable(true);

//		depth = (EditText) findViewById(R.id.DiveDialogDepth);
		time = (EditText) findViewById(R.id.DiveDialogTime);
//		setPoint = (EditText) findViewById(R.id.DiveDialogSetPoint);
		depth.setText("" + (int)dive.getDepth());
		time.setText("" + (int)dive.getTime());
		setPoint.setText("" + dive.getSetpoint());
		gases = (Spinner) findViewById(R.id.DiveDialogGases);
		ok = (Button) findViewById(R.id.DiveDialogOk);
		cancel = (Button) findViewById(R.id.DiveDialogCancel);
		cancel.setOnClickListener(cancelListener);
		ok.setOnClickListener(okListener);

		List<Gas> prefGases = MvplanInstance.getPrefs().getPrefGases();
		List<Gas> prefGasesEnabled = new ArrayList<Gas>();
		int i = 0, pos = -1 ;
		for (Gas gas : prefGases) {
			prefGasesEnabled.add(gas);
			if ( dive.getGas() != null ) {
				if (gas.getShortName().intern() == dive.getGas().getShortName().intern()) {
					pos = i; } }
			i = i + 1 ;
		}

		gases.setAdapter(new ArrayAdapter<Gas>(this.getContext(),
				android.R.layout.simple_spinner_item, prefGasesEnabled));
		if ( pos != -1 ) { gases.setSelection(pos,false); }

		LayoutParams params = getWindow().getAttributes();
		// params.height = LayoutParams.FILL_PARENT;
		params.width = LayoutParams.FILL_PARENT;
		getWindow().setAttributes(
				(android.view.WindowManager.LayoutParams) params);
		//
		super.onCreate(savedInstanceState);

	}

	private class DiveListAdaptor extends ArrayAdapter<DiveAbstract> {

		public DiveListAdaptor(Context context, int resource,
				int textViewResourceId, List<DiveAbstract> objects) {
			super(context, resource, textViewResourceId);

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View a = super.getView(position, convertView, parent);

			// handle checkbox;
			CheckBox cb = (CheckBox) a
					.findViewById(R.id.gas_list_label_check_box);
			cb.setTag(new Integer(position));
			cb.setChecked(getItem(position).getEnable());
			return a;
		}

	}

	View.OnClickListener cancelListener = new View.OnClickListener() {
		public void onClick(View v) {
			DiveDialog.this.dismiss();
		}

	};

	View.OnClickListener okListener = new View.OnClickListener() {
		public void onClick(View v) {
			try {
				double d = Double.parseDouble(depth.getText().toString());
				dive.setDepth(d);
			} catch (NumberFormatException e) {
				Toast.makeText(getContext(),
						"Could not parse Depth Setting it to default : 10",
						Toast.LENGTH_LONG);
				dive.setDepth(10);
			}
			try {
				double t = Double.parseDouble(time.getText().toString());
				dive.setTime(t);
			} catch (NumberFormatException e) {
				Toast.makeText(getContext(),
						"Could not parse Time Setting it to default : 10", Toast.LENGTH_LONG);
				dive.setTime(10);
			}
			try {
				if (setPoint.getText() == null
						|| setPoint.getText().toString().trim().equals("")) {
					dive.setSetpoint(0);
				} else {
					double sp = Double.parseDouble(setPoint.getText()
							.toString());
					dive.setSetpoint(sp);
				}
			} catch (NumberFormatException e) {
				Toast.makeText(getContext(),
						"Could not parse Set Point Setting it to  : 0", Toast.LENGTH_LONG);
				dive.setSetpoint(0);

			}
			dive.setGas((Gas) gases.getSelectedItem());

			DiveDialog.this.callback.notify(dive);
			DiveDialog.this.dismiss();

		}

	};

}
