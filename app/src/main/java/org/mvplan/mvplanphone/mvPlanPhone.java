/*
 * DivePlanner.java
 *
 *   This program is part of MV-Plan
 *   (c) 2006 Guy Wittig <>
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */

package org.mvplan.mvplanphone;

import java.util.ResourceBundle;

import mvplan.main.IMvplan;
import mvplan.main.MvplanInstance;
import mvplan.prefs.Prefs;
import android.app.Application;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

public class mvPlanPhone extends Application implements IMvplan{
	public static final String TAG = "org.mvplan.mvplanphone";
	private Prefs prefs;
	private ResourceBundle bundle;
	@Override
	public void onCreate() {
		MvplanInstance.setMvplan(this);
		prefs = new Prefs();
		//note: erics different path (reason ?)
		bundle = ResourceBundle.getBundle("src/main/resources/mvplan/resources/strings");
		prefs.setDefaultPrefs();
		
		//Mvplan.prefs.setUnitsTo(Prefs.IMPERIAL);
		super.onCreate();
		try {
			Log.d(TAG,"Starting mvplan version: "+ getPackageManager().getPackageInfo( "org.mvplan.mvplanphone",0 ).versionName);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		Log.d(TAG,"Initializing application mvPlanPhone done");
	}
	@Override
	public void onTerminate() {
		// TODO Auto-generated method stub
		super.onTerminate();
	}
	
	public String getResource(String res) {
		return bundle.getString(res);
	}
	public String getAppName() {
		String AppName = MvplanInstance.NAME+ " " + MvplanInstance.getVersion().toString()+ "  -  mvPlanPhone " + BuildConfig.VERSION_NAME;
		return AppName;
	}
	public Prefs getPrefs() {
		return prefs;
	}
	public void setPrefs(Prefs p) {
		prefs=p;
		
	}
	public int getDebug() {
		return 0;
	}
	public void init() {
		// TODO Auto-generated method stub
		
	}
	
	
}
