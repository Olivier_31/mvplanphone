/*
 * ProfileView.java
 *
 *   This program is part of MV-Plan
 *   (c) 2006 Guy Wittig <>
 *   (c) 2009-2013 Maciej Kaniewski - firegnom <>
 *   (c) 2020 Olivier Soussiel <olivier.soussiel@hotmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   The GNU General Public License can be read at http://www.gnu.org/licenses/licenses.html
 */

package org.mvplan.mvplanphone;

import mvplan.dive.Profile;
import mvplan.dive.printer.TextProfilePrinter;
import mvplan.main.MvplanInstance;
import android.os.Bundle;
import android.widget.TextView;

public class ProfileView extends mvPlanPhoneActivity {

	TextView view;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_text_view);
		view = (TextView) findViewById(R.id.profile_text_view_text);

	}

	public void calculate() {
		Profile p = new Profile(MvplanInstance.getPrefs().getPrefSegments(),
				MvplanInstance.getPrefs().getPrefGases(), null);
		// m.printModel();
		int returnCode = p.doDive();
		Profile currentProfile;
		switch (returnCode) {
		case Profile.SUCCESS:
			currentProfile = p; // Save as current profile
			p.doGasCalcs(); // Calculate gases
			// Save Model
			// Model currentModel = p.getModel();
			// System.out.println("Dive Metadata:"+currentModel.getMetaData());
			StringBuffer b = new StringBuffer();
			new TextProfilePrinter(currentProfile, b).print();
            view.setTextAppearance(this, R.style.ResultingProfile);
			view.setText(b);
			break;

		case Profile.CEILING_VIOLATION:
			view.setText(MvplanInstance.getMvplan()
					.getResource("mvplan.gui.MainFrame.ceilingViolation.text"));
			break;

		case Profile.NOTHING_TO_PROCESS:
			view.setText(MvplanInstance.getMvplan()
					.getResource("mvplan.gui.MainFrame.noSegments.text"));
			break;
		case Profile.PROCESSING_ERROR:
			view.setText(MvplanInstance.getMvplan()
					.getResource("mvplan.gui.MainFrame.processingError.text"));
		case Profile.INFINITE_DECO:
			view.setText(MvplanInstance.getMvplan()
					.getResource("mvplan.gui.MainFrame.decoNotPossible.text"));

		default:
			view.setText("Can not calculate");
			break;

		}
	}
	
}
