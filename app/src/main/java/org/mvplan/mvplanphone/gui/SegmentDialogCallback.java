package org.mvplan.mvplanphone.gui;

import mvplan.segments.SegmentAbstract;

public interface SegmentDialogCallback {
	public void notify(SegmentAbstract segment);
}
